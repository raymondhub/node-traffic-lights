TRAFFIC LIGHTS SIMULATOR

DESIGN DECISIONS

1. Trying to think and come up with the simplest solution to problems first (simplicity is the ultimate sophistication)
2. Object-oriented design in mind as it works well in test-driven development
3. Modular approach for the ease of maintainability and scalability
4. Clean and well-documented source code linted by standardjs: Use JavaScript Standard Style (https://standardjs.com)

ASSUMPTIONS

1. The traffic lights are designed for simple intersection.
2. Starting state of the traffic lights: northsouth=green eastwest=red
3. As soon as a set of traffic lights have turned red, its adjacent orthogonal set of traffic lights INSTANTLY turn green.
4. The lights changing automatically every 5 minutes is construed as
   the lights will change from green to yellow to red every 5 minutes (300 seconds) e.g.
   starts at


   1 at 0 second northsouth=green eastwest=red
   2 at 270 seconds northsouth=yellow eastwest=red
   3 at 300 seconds northsouth=red eastwest=green
   4 at 570 seconds northsouth=red eastwest=yellow
   5 at 600 seconds repeat back to start from the step 1


REQUIREMENTS

1. Node 6.12.0 or above
2. NPM
3. JEST JavaScript Testing Library


HOW TO RUN THE UNIT TESTS:

1. npm test


HOW TO SET UP, RUN AND TEST THE APPLICATION VIA THE COMMAND LINE (LINUX/MAC)

1. Git clone this repository into your local machine
2. Get into the cloned folder: cd sc-traffic-lights
2. Run npm install
4. Run the app: npm run prod
5. Once the app is up and running, the instruction/question is prompted and please provide the interval (100/10/1) as an answer (or leave it blank as default for 30 minute period).

Note for interval as an input:
To simulate the traffic lights as required 30 min period, the interval needs to be 1000 (or leave it blank as 1000 is the default interval value) and the app will take 30 minutes to simulate the traffic lights at an intersection as per requirement. However, for time-saving purposes, the interval can be specifically configured to 100 or 10 or 1 at the start of the running aplication, which means the traffic lights simulation will run 10 or 100 or 1000 times faster than 30 minute period initially required (per millisecond interval).

The example of the running app (30 minute duration):

raymonds-Mac:sc-traffic-lights raymond$ npm run prod

> sc-traffic-lights@1.0.0 prod /Users/raymond/Documents/traffic-lights-example/sc-traffic-lights
> node app.js


Please provide the interval for traffic lights simulator to run (e.g: default in 1000 ms. configurable :100/10/1 ms interval): 1000
Configuring..ready..go!

Wed Nov 22 2017 13:48:31 GMT+1100 (AEDT) : NORTH-SOUTH traffic: GREEN and EAST-WEST traffic: RED

Wed Nov 22 2017 13:53:02 GMT+1100 (AEDT) : NORTH-SOUTH traffic: YELLOW and EAST-WEST traffic: RED

Wed Nov 22 2017 13:53:32 GMT+1100 (AEDT) : NORTH-SOUTH traffic: RED and EAST-WEST traffic: GREEN

Wed Nov 22 2017 13:58:02 GMT+1100 (AEDT) : NORTH-SOUTH traffic: RED and EAST-WEST traffic: YELLOW

Wed Nov 22 2017 13:58:33 GMT+1100 (AEDT) : NORTH-SOUTH traffic: GREEN and EAST-WEST traffic: RED

Wed Nov 22 2017 14:03:03 GMT+1100 (AEDT) : NORTH-SOUTH traffic: YELLOW and EAST-WEST traffic: RED

Wed Nov 22 2017 14:03:33 GMT+1100 (AEDT) : NORTH-SOUTH traffic: RED and EAST-WEST traffic: GREEN

Wed Nov 22 2017 14:08:03 GMT+1100 (AEDT) : NORTH-SOUTH traffic: RED and EAST-WEST traffic: YELLOW

Wed Nov 22 2017 14:08:33 GMT+1100 (AEDT) : NORTH-SOUTH traffic: GREEN and EAST-WEST traffic: RED

Wed Nov 22 2017 14:13:04 GMT+1100 (AEDT) : NORTH-SOUTH traffic: YELLOW and EAST-WEST traffic: RED

Wed Nov 22 2017 14:13:34 GMT+1100 (AEDT) : NORTH-SOUTH traffic: RED and EAST-WEST traffic: GREEN

Wed Nov 22 2017 14:18:04 GMT+1100 (AEDT) : NORTH-SOUTH traffic: RED and EAST-WEST traffic: YELLOW


Completed.
Thank you

raymonds-Mac:sc-traffic-lights raymond$
