'use strict'
const { TrafficLights } = require('./traffic-lights')

class Intersection {
  constructor () {
    this.northSouth = new TrafficLights()
    this.eastWest = new TrafficLights()
    this.RED = 0
  }

 /*
  * Setter for north south traffic lights
  * @param state
  */
  setNorthSouth (state = this.RED) {
    this.northSouth.setState(state)
  }

 /*
  * Getter for north south traffic lights
  *
  */
  getNorthSouth () {
    return this.northSouth.getState()
  }

 /*
  * Setter for east west traffic lights
  * @param state
  */
  setEastWest (state = this.RED) {
    this.eastWest.setState(state)
  }

 /*
  * Getter for north south traffic lights
  *
  */
  getEastWest () {
    return this.eastWest.getState()
  }

 /*
  * Safety check for the state of the north south traffic lights
  *
  */
  collisionAvoidanceNorthSouth () {
    if (this.getNorthSouth() !== this.RED) {
      this.setEastWest(this.RED)
    }
  }

 /*
  * Safety check for the state of the east west traffic lights
  *
  */
  collisionAvoidanceEastWest () {
    if (this.getEastWest() !== this.RED) {
      this.setNorthSouth(this.RED)
    }
  }

 /*
  * Show all traffic lights information
  *
  */
  allTrafficLights () {
    let now = new Date()
    let status = `${now} : NORTH-SOUTH traffic: ${this.northSouth.getLightColour()} and EAST-WEST traffic: ${this.eastWest.getLightColour()}\n`
    return status
  }
}

exports.Intersection = Intersection
