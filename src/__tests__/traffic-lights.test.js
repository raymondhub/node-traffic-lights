'use strict'
const { TrafficLights } = require('../traffic-lights')

const trafficLights = new TrafficLights()

test('The default light state is 0', () => {
  let lightState = trafficLights.getState()
  expect(lightState).toBe(0)
})

test('The default light colour is RED', () => {
  let lightColour = trafficLights.getLightColour()
  expect(lightColour).toBe('RED')
})

test('Set light state to two to equal to 2', () => {
  trafficLights.setState(2)
  let lightState = trafficLights.getState()
  expect(lightState).toBe(2)
})

test('Set light state to one NOT to equal to 2', () => {
  trafficLights.setState(1)
  let lightState = trafficLights.getState()
  expect(lightState).not.toBe(2)
})

test('Set light colour to 1 to equal to YELLOW', () => {
  trafficLights.setState(1)
  let lightColour = trafficLights.getLightColour()
  expect(lightColour).toBe('YELLOW')
})

test('Set light colour to 0 NOT to equal to GREEN', () => {
  trafficLights.setState(0)
  let lightColour = trafficLights.getLightColour()
  expect(lightColour).not.toBe('GREEN')
})
