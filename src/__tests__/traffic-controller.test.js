'use strict'
const { TrafficController } = require('../traffic-controller')

describe('Intersection traffic lights starts at the green light of NORTH-SOUTH traffic and the red light of EAST-WEST traffic with the interval of 1 second', () => {
  const trafficController = new TrafficController('intersection', 180, 30, 270, 1000)
  let intialTrafficLightsState = trafficController.intialTrafficLightsState()
  test('The intial traffic lights state is ', () => {
    expect(intialTrafficLightsState).toMatch(/NORTH-SOUTH traffic: GREEN/)
    expect(intialTrafficLightsState).toMatch(/EAST-WEST traffic: RED/)
  })

  jest.useFakeTimers()
  test('the traffic light at intersection runs at the correct interval', () => {
    trafficController.run()
    expect(setInterval.mock.calls.length).toBe(1)
    // Test if it runs at 1 second interval
    expect(setInterval.mock.calls[0][1]).toBe(1000)
  })
})
