'use strict'
const { Intersection } = require('../intersection')

const intersection = new Intersection()

test('The default NORTH-SOUTH traffic lights is 0', () => {
  let northSouth = intersection.getNorthSouth()
  expect(northSouth).toBe(0)
})

test('The default EAST-WEST traffic lights is 0', () => {
  let eastWest = intersection.getEastWest()
  expect(eastWest).toBe(0)
})

test('Set EAST-WEST traffic lights to two to equal to 2', () => {
  intersection.setEastWest(2)
  let eastWest = intersection.getEastWest()
  expect(eastWest).toBe(2)
})

test('Set EAST-WEST traffic lights to 0ne NOT to equal to 2', () => {
  intersection.setEastWest(1)
  let eastWest = intersection.getEastWest()
  expect(eastWest).not.toBe(2)
})

test('Set NORTH-SOUTH traffic lights to one to equal to 1', () => {
  intersection.setNorthSouth(1)
  let northSouth = intersection.getNorthSouth()
  expect(northSouth).toBe(1)
})

test('Set NORTH-SOUTH traffic lights to zero NOT to equal to 2', () => {
  intersection.setNorthSouth(0)
  let northSouth = intersection.getNorthSouth()
  expect(northSouth).not.toBe('GREEN')
})

test('Collison Avoidance test: If NORTH-SOUTH traffic lights is GREEN or YELLOW then EAST-WEST traffic lights must be RED and vice versa', () => {
  intersection.setNorthSouth(1)
  intersection.collisionAvoidanceNorthSouth()
  expect(intersection.allTrafficLights()).toMatch(/NORTH-SOUTH traffic: YELLOW/)
  expect(intersection.allTrafficLights()).toMatch(/EAST-WEST traffic: RED/)
  intersection.setNorthSouth(2)
  intersection.collisionAvoidanceNorthSouth()
  expect(intersection.allTrafficLights()).toMatch(/NORTH-SOUTH traffic: GREEN/)
  expect(intersection.allTrafficLights()).toMatch(/EAST-WEST traffic: RED/)
  intersection.setEastWest(1)
  intersection.collisionAvoidanceEastWest()
  expect(intersection.allTrafficLights()).toMatch(/EAST-WEST traffic: YELLOW/)
  expect(intersection.allTrafficLights()).toMatch(/NORTH-SOUTH traffic: RED/)
  intersection.setEastWest(2)
  intersection.collisionAvoidanceEastWest()
  expect(intersection.allTrafficLights()).toMatch(/EAST-WEST traffic: GREEN/)
  expect(intersection.allTrafficLights()).toMatch(/NORTH-SOUTH traffic: RED/)
})
