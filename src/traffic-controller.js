'use strict'
const { Intersection } = require('./intersection')

class TrafficController {
  constructor (type = 'intersection', duration = 3600, yellowDuration = 60,
              redGreenDuration = 540, interval = 1000) {
    this.type = type
    this.interval = interval
    this.duration = duration
    this.changeDuration = redGreenDuration + yellowDuration
    this.redGreenDuration = redGreenDuration
    this.timer = 0
    this.intersection = new Intersection()
    this.RED = 0
    this.YELLOW = 1
    this.GREEN = 2
  }

  intialTrafficLightsState () {
    this.intersection.setNorthSouth(this.GREEN)
    this.intersection.setEastWest(this.RED)
    return this.intersection.allTrafficLights()
  }

  run () {
    console.log(this.intialTrafficLightsState())
    let simulatorTime = setInterval(() => {
      let northSouthState = this.intersection.getNorthSouth()
      if (northSouthState !== this.RED) {
        this.intersection.collisionAvoidanceNorthSouth()
        if (this.timer === this.redGreenDuration) {
          this.intersection.setNorthSouth(this.YELLOW)
          console.log(this.intersection.allTrafficLights())
        } else if (this.timer > this.redGreenDuration) {
          if (this.timer === this.changeDuration) {
            this.intersection.setNorthSouth(this.RED)
            this.intersection.setEastWest(this.GREEN)
            console.log(this.intersection.allTrafficLights())
            this.timer = 0
          }
        }
      } else {
        if (this.timer === this.redGreenDuration) {
          this.intersection.setEastWest(this.YELLOW)
          this.intersection.collisionAvoidanceEastWest()
          console.log(this.intersection.allTrafficLights())
        } else if (this.timer > this.redGreenDuration) {
          if (this.timer === this.changeDuration) {
            this.intersection.setEastWest(this.RED)
            this.intersection.setNorthSouth(this.GREEN)
            console.log(this.intersection.allTrafficLights())
            this.timer = 0
          }
        }
      }
      this.timer++
      if (this.timer === this.changeDuration) { this.duration -= this.timer }
      if (this.duration < 1) {
        clearInterval(simulatorTime)
        console.log(`\nCompleted.\nThank you\n`)
      }
    }, this.interval)
  }
}

exports.TrafficController = TrafficController
