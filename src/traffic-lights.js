'use strict'

class TrafficLights {
  constructor () {
    this.state = 0 // starts with red a.k.a the safest
    this.lights = ['RED', 'YELLOW', 'GREEN']
  }

 /*
  * Setter for traffic light
  * @param state
  */
  setState (state) {
    // console.log(state)
    this.state = state
  }

 /*
  * Getter for traffic light
  *
  */
  getState () {
    return this.state
  }

 /*
  * Getter for traffic light colour
  *
  */
  getLightColour () {
    return this.lights[this.state]
  }
}

exports.TrafficLights = TrafficLights
