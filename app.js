'use strict'
const readline = require('readline')
const { TrafficController } = require('./src/traffic-controller')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question(`\nPlease provide the interval for traffic lights simulator to run (e.g: default in 1000 ms. configurable :100/10/1 ms interval): `, (interval) => {
  console.log(`Configuring..ready..go!\n`)

  let validInterval = [1000, 100, 10, 1]
  let duration = 1800 // 30 mins
  let yellowDuration = 30
  let redGreenDuration = 270
  interval = parseInt(interval)
  if (!validInterval.includes(interval)) { interval = 1000 } // default 1 sec
  let intersectionTrafficLights = new TrafficController('intersection', duration,
                                      yellowDuration, redGreenDuration, interval)
  intersectionTrafficLights.run()
  rl.close()
})
